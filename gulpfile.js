const project_folder = "dist";
const source_folder = "src";

const fs = require('fs');

const path = {
  build: {
    html: project_folder + "/",
    css: project_folder + "/css/",
    js: project_folder + "/js/",
    img: project_folder + "/img/",
    fonts: project_folder + "/fonts/",
  },
  src: {
    html: source_folder + "/*.html",
    css: source_folder + "/scss/style.scss",
    js: source_folder + "/js/*.js",
    img: source_folder + "/img/**/*.{jpg,png,svg,gif,ico,webp}",
    fonts: source_folder + "/fonts/*.{ttf,woff,woff2}",
  },
  watch: {
    html: source_folder + "/**/*.html",
    css: source_folder + "/scss/**/*.scss",
    js: source_folder + "/js/**/*.js",
    img: source_folder + "/img/**/*.{jpg,png,svg,gif,ico,webp}",
  },
  clean: "./" + project_folder + "/"
}


const {src, dest} = require('gulp');
const gulp = require('gulp');
const webp = require('gulp-webp');
const plumber = require('gulp-plumber');
const browsersync = require('browser-sync').create();
const del = require('del');
const scss = require('gulp-sass');
const autoprefixer = require('gulp-autoprefixer');
const clean_css = require('gulp-clean-css');
const sourcemap = require('gulp-sourcemaps');
const rename = require('gulp-rename');
const babel = require('gulp-babel');
const svgSprite = require('gulp-svg-sprite');


const browserSync = (params) => {
  browsersync.init({
    server: {
      baseDir: "./" + project_folder + "/"
    },
    cors: true,
    port: 3000,
    notify: false
  })
}

const html = () => {
  return src(path.src.html)
    .pipe(dest(path.build.html))
    .pipe(browsersync.stream())
}

const css = () => {
  return src(path.src.css)
    .pipe(plumber())
    .pipe(sourcemap.init())
    .pipe(
      scss({
        outputStyle: "expanded"
      })
    )
    .pipe(
      autoprefixer({
        cascade: true
      })
    )
    .pipe(sourcemap.write('.'))
    .pipe(dest(path.build.css))
    .pipe(clean_css())
    .pipe(
      rename({
        extname: '.min.css'
      })
    )
    .pipe(sourcemap.write('.'))
    .pipe(dest(path.build.css))
    .pipe(browsersync.stream())
}

const js = () => {
  return src(path.src.js)
    .pipe(babel({
      presets: ['@babel/env']
    }))
    .pipe(dest(path.build.js))
    .pipe(browsersync.stream())
}

const img = () => {
  return src(path.src.img)
    .pipe(dest(path.build.img))
    .pipe(webp())
    .pipe(dest(path.build.img))
}

const svg = () => {
  return gulp.src([source_folder + '/img/icon-*.svg'])
    .pipe(svgSprite({
      mode: {
        stack: {
          sprite: '../sprite.svg',
          // example: true
        }
      },
    }))
    .pipe(dest(path.build.img))
}

const font = () => {
  return src(path.src.fonts)
    .pipe(dest(path.build.fonts))
}

const watchFiles = (params) => {
  gulp.watch([path.watch.html], html);
  gulp.watch([path.watch.css], css);
  gulp.watch([path.watch.js], js);
  gulp.watch([path.watch.img], img);
}

const clean = (params) => {
  return del(path.clean);
}


let build = gulp.series(clean, gulp.parallel(html, css, js, img, svg, font));
let watch = gulp.parallel(gulp.series(build, browserSync), watchFiles);


exports.html = html;
exports.css = css;
exports.js = js;
exports.img = img;
exports.svg = svg;
exports.font = font;
exports.build = build;
exports.watch = watch;
exports.default = watch;
